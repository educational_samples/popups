'''
Created on 7 jul. 2020

# maya start up
path = r'D:\work\eclipse_workspace'  # <- path to where the tool folder is


import sys
sys.path.append(path)


from popups import simple_dialog_popup
reload(simple_dialog_popup)  # <- to take changes that you made
ui = simple_dialog_popup.main()  # need to store the dialog in the ui variable so it's not garbage collected



@author: eduardo
'''
import sys
import os
import logging

from PySide2 import QtWidgets, QtCore

from maya import cmds, mel

import loadUiType
import random



# set a logger for the script!
logger = logging.getLogger('cache_export.simple_dialog')
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

# create base class from ui file
MAIN_UI_FULL_PATH = os.path.join(os.path.dirname(__file__), 'ui\\simple.ui')
MAIN_FORM_CLASS, MAIN_BASE_CLASS = loadUiType.loadUiType(MAIN_UI_FULL_PATH)


class MainUI(MAIN_FORM_CLASS, MAIN_BASE_CLASS):
    ''' This class implements the main ui '''

    def __init__(self, template_sel=None, parent=None):

        # configure parent class and apply ui definition
        super(MainUI, self).__init__(parent=parent)
        self.setupUi(self)

        # adds the context menu setup to button and label
        self.addContextMenu()
        self.addDynamicContextMenu()

    def addContextMenu(self):
        ''' This gets populated on ui load '''
        logger.info('CREATING static context menu')

        # set the context manu for the button as an actions context menu
        self.pushButton.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)

        # add the actions
        action = QtWidgets.QAction(self.pushButton)
        action.setText('Say Hello')
        action.triggered.connect(self.sayHello)
        self.pushButton.addAction(action)

        action = QtWidgets.QAction(self.pushButton)
        action.setText('Say Good bye')
        action.triggered.connect(self.sayGoodBye)
        self.pushButton.addAction(action)

    def sayHello(self):
        cmds.confirmDialog(message='Hello!')

    def sayGoodBye(self):
        cmds.confirmDialog(message='Good Bye!')

    def addDynamicContextMenu(self):
        # override the default contextMenuEvent with our own
        self.label.contextMenuEvent = self.myContextMenu

    def myContextMenu(self, event):
        ''' This gets populated when you right click
        Since we are overwriting a default method, we need to respect the function signature and
        keept the 'event' paramenter. this will be usefull later to position the menu properly
        '''
        logger.info('CREATING dynamic context menu NOW!')

        # create the menu
        menu = QtWidgets.QMenu()

        random_number = random.randint(1, 5)

        action = QtWidgets.QAction(self.label)
        action.setText('adding {} times'.format(random_number))
        action.setEnabled(False)
        menu.addAction(action)

        for i in range(random_number):  # @UnusedVariable
            # add the actions
            action = QtWidgets.QAction(self.label)
            action.setText('Say Hello')
            action.triggered.connect(self.sayHello)
            menu.addAction(action)

            action = QtWidgets.QAction(self.label)
            action.setText('Say Good bye')
            action.triggered.connect(self.sayGoodBye)
            menu.addAction(action)

            menu.addSeparator()

        # execute the manu at the position the the right click (event) was produced
        menu.exec_(event.globalPos())


def getMayaMainWindowAsQWidget():
    ''' Returns the maya main window as a Pyside.QWidget '''

    # get pointer to main window object in memory
    from maya import OpenMayaUI as omui
    ptr = omui.MQtUtil.mainWindow()

    # wrap the object at that memory location with a QWidget
    from PySide2 import QtWidgets
    from shiboken2 import wrapInstance
    widget = wrapInstance(long(ptr), QtWidgets.QWidget)

    # return the qwidget
    return widget


def main():
    # get parent (maya window)
    parent = getMayaMainWindowAsQWidget()

    # create the ui and show it
    ui = MainUI(parent=parent)
    ui.show()

    # return it so it is not garbage collected
    return ui
